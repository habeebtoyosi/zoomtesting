﻿using Nest;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace ElasticSearch
{
    class Program
    {
        static Uri local = new Uri("http://localhost:9200");
        static ConnectionSettings settings = new ConnectionSettings(local, "zoom");
        static ElasticClient elastic = new ElasticClient(settings);

        public static bool CreateIndex()
        {
            var res = elastic.CreateIndex(ci => ci.Index("zoom")
               .AddMapping<SBU>(m => m.MapFromAttributes())
               .AddMapping<TaskFeed>(m => m.MapFromAttributes().SetParent<SBU>())
               .AddMapping<Commits>(m => m.MapFromAttributes()));

            return res.RequestInformation.Success;
        }

        public static bool InsertSBU()
        {
            var descriptor = new BulkDescriptor();
            var sbus = new List<SBU>();
            string[] sbunames = { "AVITECH", "POWERTECH", "FUELTECH", "CORPORATE", "EDUTECH", "FINATECH", "EBIPS", "EDUPORTAL", "NO CATEGORY" };
            int i = 0;
            foreach (var name in sbunames)
            {
                descriptor.Index<SBU>(sb => sb.Document(new SBU
                {
                    id = i++.ToString(),
                    name = name
                }));
            }

            var bulkresult = elastic.Bulk(descriptor);
            return bulkresult.RequestInformation.Success;
        }

        public static bool InsertJiraArtemisMap()
        {
            var descriptor = new BulkDescriptor();
            var s = new List<string[]>();

            s.Add(new string[] { "AMR", "amr", "665564" });
            s.Add(new string[] { "Application forms", "apf", "599134" });
            s.Add(new string[] { "Automation Project", "atp", "605971" });
            s.Add(new string[] { "Aviation Data", "ad", "689432" });
            s.Add(new string[] { "Avicollect", "av", "676389" });
            s.Add(new string[] { "Avicollect API", "aa", "693627" });
            s.Add(new string[] { "Avicollect Data entry", "ade", "671090" });
            s.Add(new string[] { "Avicollect Domestic", "tsc", "500491" });
            s.Add(new string[] { "Avicollect International", "fid", "500701" });
            s.Add(new string[] { "Avicollect Processor", "avp", "688398" });
            s.Add(new string[] { "Billing Data Aggregator", "bda", "499324" });
            s.Add(new string[] { "Central Toll management Portal", "ctmp", "693201" });
            s.Add(new string[] { "Electronic Cash Register", "ecr", "693628" });
            s.Add(new string[] { "Eduportal", "ed", "445085" });
            s.Add(new string[] { "Electricity Metering System", "ems", "530923" });
            s.Add(new string[] { "Fuel Monitoring System", "fms", "506939" });
            s.Add(new string[] { "Flight Management Data", "fmd", "675586" });
            s.Add(new string[] { "Learning Management System", "Lms", "604288" });
            s.Add(new string[] { "Monitoring Dashboard", "mdb", "687384" });
            s.Add(new string[] { "Obstacle Clearance", "oc", "516536" });
            s.Add(new string[] { "PowerCollect", "pow", "538699" });
            s.Add(new string[] { "PowerPay", "ppy", "645553" });
            s.Add(new string[] { "securetech", "st", "688901" });
            s.Add(new string[] { "single sign on", "sso", "674753" });
            s.Add(new string[] { "ZOOM", "zoom", "692805" });

            foreach (var item in s)
            {
                descriptor.Index<JiraToArtemis>(mp => mp.Document(new JiraToArtemis
                {
                    project_name = item[0],
                    project_key = item[1],
                    repo_id = item[2]
                }));
            }

            var bulkresult = elastic.Bulk(descriptor);
            return bulkresult.RequestInformation.Success;
        }

        public static bool InsertTasks()
        {
            var descriptor = new BulkDescriptor();

            string U_ID = "kasali.olawale";
            string U_SECRET = ".alozwaly+1";

            string username = U_ID;
            string password = U_SECRET;
            string[] projects = { "ADSB", "AMR", "AMLG", "APF", "AP", "ATP", "AD", "AF", "AV", "AA", "ADE", "TSC", "FID", "AVP", "BDA", "CARGO", "CTMP", "CHAR", "CLOC", "DEMO", "DOC", "ED", "EMS", "ECR", "VAT", "FSD", "FMD", "FMS", "GEEP", "HNI", "LSEB", "LMS", "MDB", "NTAP", "OPXDS", "OC", "OP", "PMD", "POW", "MIR", "PPY", "RL", "ST", "SSO", "SPRYTE", "SIS", "SWIP", "TESPRJ", "TAP", "TRIP", "VPOC", "VGA", "VGD", "ZOOM" };
            var result = string.Empty;
            var results = new List<TaskFeed>();

            JiraManager manager = new JiraManager(username, password);

            foreach (var project in projects)
            {
                string ARG = "='" + project + "'";
                result = manager.RunQuery(JiraResource.project, ARG);
                var resultdata = JObject.Parse(result)["issues"].ToObject<List<Issues>>();
                var resultfields = resultdata.Select(x => x).Where(x => x.fields.assignee != null).ToList();
                foreach (var item in resultfields)
                {
                    if (object.ReferenceEquals(null, item.fields.project.projectCategory))
                    {
                        continue;
                    }
                    else
                    {
                        //var idd = Guid.NewGuid().ToString();
                        descriptor.Index<TaskFeed>(t => t.Document(new TaskFeed
                        {
                            Id = item.id,
                            project_key = item.fields.project.key,
                            project_name = item.fields.project.name,
                            project_Category = item.fields.project.projectCategory.name,
                            user_email = item.fields.assignee.emailAddress,
                            user_name = item.fields.assignee.displayName,
                            user_avatar = item.fields.assignee.avatarUrls.__invalid_name__48x48,
                            status = item.fields.status.statusCategory.name,
                            summary = item.fields.summary,
                            created = item.fields.created,
                            updated = item.fields.updated
                        }).Parent(item.fields.project.projectCategory.name));
                        Console.WriteLine(item.fields.assignee.avatarUrls.__invalid_name__48x48);
                    }
                }
            }
            var indexResult = elastic.Bulk(descriptor);
            return indexResult.RequestInformation.Success;
        }

        public static bool InsertCommits()
        {
            var descriptor = new BulkDescriptor();
            string username = "razaq";
            string password = ".alozwaly+1";

            JiraManager manager = new JiraManager(username, password);
            var commitList = manager.GetCommits();
            foreach (var item in commitList)
            {
                Console.WriteLine("{0} {1} {2} {3} {4}", item.author, item.repo_id, item.revision, item.Time, item.email);
                descriptor.Index<Commit>(c => c.Document(item));
            }
            var result = elastic.Bulk(descriptor);
            return result.RequestInformation.Success;
        }

        public static void SearchTasks(string sbu = null)
        {
            var result = elastic.Search<TaskFeed>(c => c
                .Type("tasks")
                .Size(50)
                .Query(q => q.MatchAll())
                .Filter(f => f
                .Term(t => t.project_Category, sbu)));
            /*.Filter(f => f
                .HasParent<SBU>(s => s
                    .Query(cq => cq.Term(t => t.name, "AVITECH")))));*/

            var keyId = GetKeysID();
            var commits = GetCommits();
            var grps = commits.GroupBy(x => new { x.email, x.repo_id }).ToList();

            foreach (var item in result.Documents.GroupBy(x => new { x.user_email, x.project_key }).ToList())
            {
                try
                {
                    var info = commits.Select(y => y).Where(y => y.email == item.First().user_email).ToList(); //&& keyId[y.First().repo_id] == item.First().project_name).ToList();
                    if (info != null)
                    {
                        Console.WriteLine("{0} has the task: {1}({2}) and has committed: {3} times.", item.First().user_name, item.First().summary, item.First().status, info.Count());
                        Console.WriteLine(info.Count());
                    }
                }
                catch (KeyNotFoundException ex)
                {
                    Console.WriteLine("{0} has the task: {2}-{3}", item.First().user_name, item.First().summary, item.First().status);
                }
            }
            Console.WriteLine(result.Documents.Count());
        }

        public static IEnumerable<TaskFeed> GetTasks(string email = null)
        {
            var result = elastic.Search<TaskFeed>(a => a
                .Size(50)
                .Type("tasks")
                .Query(y => y.Term(p => p.user_email, email)));

            foreach (var item in result.Documents)
            {
                Console.WriteLine("Name of proj is " + item.project_name);
            }
            return result.Documents;
        }

        public static IEnumerable<Commit> GetCommits()
        {
            var dict = GetKeysID();
            var result = elastic.Search<Commit>(c => c
                .Size(15)
                .Type("commits")
                .Query(q => q.MatchAll()));

            //string p_Key;
            var commitGrp = result.Documents.GroupBy(x => new { x.email, x.repo_id });
            /*foreach (var commit in commitGrp)
            {
                try
                {
                    p_Key = dict[commit.First().repo_id];
                    Console.WriteLine("{0}({1}) committed to project: {2}({3}) {4} times", commit.First().author, commit.First().email, p_Key, commit.First().repo_id, commit.Count());
                }
                catch (Exception ex)
                {
                    p_Key = "No defined project";
                    Console.WriteLine("{0}({1}) committed to project: {2} {3} times", commit.First().author, commit.First().email, p_Key, commit.Count());
                }
            }*/
            return result.Documents;
        }

        public static Dictionary<string, string> GetKeysID()
        {
            var dict = new Dictionary<string, string>();

            var result = elastic.Search<JiraToArtemis>(j => j
                .Type("pKeyRepoID")
                .Size(24)
                .Query(q => q.MatchAll()));

            foreach (var item in result.Documents)
            {
                dict.Add(item.repo_id, item.project_name);
                //dict.Add(item.project_key, item.repo_id);
                //Console.WriteLine("{0}: {1} {2}", item.repo_id, item.project_name, item.project_key);
            }
            //Console.WriteLine(dict["692805"]);
            return dict;
        }

        public static void Reload(){
            Timer timer = new Timer(120000);
            timer.AutoReset = true;
            timer.Enabled = true;
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        private static void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            try
            {
                Console.Clear();
                Console.WriteLine(InsertCommits());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        static void Main(string[] args)
        {


            //var res = elastic.Raw.ClusterHealth();
            //  Create SBU index as parent



            // Putting documents into index
            /*var blogPost = new BlogPost
            {
                Id = Guid.NewGuid(),
                Title = "First blog post",
                Body = "This is very long blog post"
            };

            //var firstId = blogPost.Id;

            var res = elastic.Index(blogPost, p => p
               .Index("my_first_index")
               .Id(blogPost.Id.ToString())
               .Refresh());

            // Retrieving docs from index by id
            //var resGet = elastic.Get<BlogPost>(g => g.Id(firstId.ToString()).Index("my_first_index"));

            Console.WriteLine(resGet.RequestInformation.Success);
            //Console.WriteLine(resGet.Source);
            //var info = resGet.Source;
            //Console.WriteLine(info.Title);*/
            //Console.WriteLine(res.SuccessOrKnownError);

            //GetCommits();
            //GetKeysID();
            //GetTasks();
            //SearchTasks();
            //Reload();
            //Console.WriteLine(InsertSBU());
            //Console.WriteLine(CreateIndex());
            //Console.WriteLine(InsertJiraArtemisMap());
            //Console.WriteLine(InsertTasks());
            Console.WriteLine(InsertCommits());
            Console.Read();
        }
    }
}