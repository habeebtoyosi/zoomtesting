﻿using System;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nest;

namespace ElasticSearch
{
    public class Issuetype
    {
        public string self { get; set; }
        public string id { get; set; }
        public string description { get; set; }
        public string iconUrl { get; set; }
        public string name { get; set; }
        public bool subtask { get; set; }
    }

    public class AvatarUrls
    {
        public string __invalid_name__48x48 { get; set; }
        public string __invalid_name__24x24 { get; set; }
        public string __invalid_name__16x16 { get; set; }
        public string __invalid_name__32x32 { get; set; }
    }

    public class ProjectCategory
    {
        public string self { get; set; }
        public string id { get; set; }
        public string description { get; set; }
        public string name { get; set; }
    }

    public class Project
    {
        public string self { get; set; }
        public string id { get; set; }
        public string key { get; set; }
        public string name { get; set; }
        public AvatarUrls avatarUrls { get; set; }
        public ProjectCategory projectCategory { get; set; }
    }

    public class Resolution
    {
        public string self { get; set; }
        public string id { get; set; }
        public string description { get; set; }
        public string name { get; set; }
    }

    public class Watches
    {
        public string self { get; set; }
        public long watchCount { get; set; }
        public bool isWatching { get; set; }
    }

    public class Priority
    {
        public string self { get; set; }
        public string iconUrl { get; set; }
        public string name { get; set; }
        public string id { get; set; }
    }

    public class AvatarUrls2
    {
        [JsonProperty("48x48")]
        public string __invalid_name__48x48 { get; set; }
        [JsonProperty("24x24")]
        public string __invalid_name__24x24 { get; set; }
        [JsonProperty("16x16")]
        public string __invalid_name__16x16 { get; set; }
        [JsonProperty("32x32")]
        public string __invalid_name__32x32 { get; set; }
    }

    public class Assignee
    {
        public string self { get; set; }
        public string name { get; set; }
        public string key { get; set; }
        public string emailAddress { get; set; }
        public AvatarUrls2 avatarUrls { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
        public string timeZone { get; set; }
    }

    public class StatusCategory
    {
        public string self { get; set; }
        public long id { get; set; }
        public string key { get; set; }
        public string colorName { get; set; }
        public string name { get; set; }
    }

    public class Status
    {
        public string self { get; set; }
        public string description { get; set; }
        public string iconUrl { get; set; }
        public string name { get; set; }
        public string id { get; set; }
        public StatusCategory statusCategory { get; set; }
    }

    public class AvatarUrls3
    {
        public string __invalid_name__48x48 { get; set; }
        public string __invalid_name__24x24 { get; set; }
        public string __invalid_name__16x16 { get; set; }
        public string __invalid_name__32x32 { get; set; }
    }

    public class Creator
    {
        public string self { get; set; }
        public string name { get; set; }
        public string key { get; set; }
        public string emailAddress { get; set; }
        public AvatarUrls3 avatarUrls { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
        public string timeZone { get; set; }
    }

    public class AvatarUrls4
    {
        public string __invalid_name__48x48 { get; set; }
        public string __invalid_name__24x24 { get; set; }
        public string __invalid_name__16x16 { get; set; }
        public string __invalid_name__32x32 { get; set; }
    }

    public class Reporter
    {
        public string self { get; set; }
        public string name { get; set; }
        public string key { get; set; }
        public string emailAddress { get; set; }
        public AvatarUrls4 avatarUrls { get; set; }
        public string displayName { get; set; }
        public bool active { get; set; }
        public string timeZone { get; set; }
    }

    public class Aggregateprogress
    {
        public long progress { get; set; }
        public long total { get; set; }
    }

    public class Progress
    {
        public long progress { get; set; }
        public long total { get; set; }
    }

    public class Votes
    {
        public string self { get; set; }
        public long votes { get; set; }
        public bool hasVoted { get; set; }
    }

    public class Fields
    {
        public Issuetype issuetype { get; set; }
        public object timespent { get; set; }
        public Project project { get; set; }
        public object customfield_13102 { get; set; }
        public List<object> fixVersions { get; set; }
        public object customfield_10110 { get; set; }
        public object customfield_10111 { get; set; }
        public object customfield_13104 { get; set; }
        public object aggregatetimespent { get; set; }
        public Resolution resolution { get; set; }
        public object customfield_10112 { get; set; }
        public object customfield_13103 { get; set; }
        public object customfield_13106 { get; set; }
        public object customfield_10113 { get; set; }
        public object customfield_13105 { get; set; }
        public object customfield_10114 { get; set; }
        public object customfield_10500 { get; set; }
        public object customfield_10104 { get; set; }
        public object customfield_12206 { get; set; }
        public object customfield_10302 { get; set; }
        public object customfield_10105 { get; set; }
        public object customfield_10501 { get; set; }
        public object customfield_10303 { get; set; }
        public object customfield_12205 { get; set; }
        public object customfield_10502 { get; set; }
        public object customfield_10106 { get; set; }
        public object customfield_10700 { get; set; }
        public object customfield_10107 { get; set; }
        public object customfield_10503 { get; set; }
        public object customfield_10701 { get; set; }
        public object customfield_10108 { get; set; }
        public object customfield_10504 { get; set; }
        public object customfield_10109 { get; set; }
        public string resolutiondate { get; set; }
        public object customfield_10903 { get; set; }
        public object customfield_10904 { get; set; }
        public long workratio { get; set; }
        public object lastViewed { get; set; }
        public Watches watches { get; set; }
        public string created { get; set; }
        public Priority priority { get; set; }
        public object customfield_10100 { get; set; }
        public object customfield_12202 { get; set; }
        public object customfield_10102 { get; set; }
        public object customfield_10300 { get; set; }
        public object customfield_12204 { get; set; }
        public List<object> labels { get; set; }
        public object customfield_10103 { get; set; }
        public object customfield_10301 { get; set; }
        public object customfield_12203 { get; set; }
        public object customfield_11303 { get; set; }
        public object customfield_13405 { get; set; }
        public object customfield_11304 { get; set; }
        public object customfield_13404 { get; set; }
        public object customfield_11305 { get; set; }
        public object customfield_11306 { get; set; }
        public object customfield_13406 { get; set; }
        public object timeestimate { get; set; }
        public object aggregatetimeoriginalestimate { get; set; }
        public List<object> versions { get; set; }
        public List<object> issuelinks { get; set; }
        public Assignee assignee { get; set; }
        public string updated { get; set; }
        public Status status { get; set; }
        public List<object> components { get; set; }
        public object timeoriginalestimate { get; set; }
        public string description { get; set; }
        public object customfield_11102 { get; set; }
        public object customfield_13403 { get; set; }
        public object customfield_11302 { get; set; }
        public object customfield_13402 { get; set; }
        public object customfield_10800 { get; set; }
        public object aggregatetimeestimate { get; set; }
        public string summary { get; set; }
        public Creator creator { get; set; }
        public List<object> subtasks { get; set; }
        public Reporter reporter { get; set; }
        public object customfield_10120 { get; set; }
        public object customfield_10000 { get; set; }
        public Aggregateprogress aggregateprogress { get; set; }
        public object customfield_10001 { get; set; }
        public object customfield_13702 { get; set; }
        public object customfield_10115 { get; set; }
        public object environment { get; set; }
        public object customfield_13704 { get; set; }
        public object customfield_13902 { get; set; }
        public object customfield_13703 { get; set; }
        public object customfield_10118 { get; set; }
        public object customfield_13904 { get; set; }
        public object customfield_10119 { get; set; }
        public object customfield_13903 { get; set; }
        public object duedate { get; set; }
        public object customfield_13906 { get; set; }
        public object customfield_13905 { get; set; }
        public Progress progress { get; set; }
        public Votes votes { get; set; }
    }

    public class Issues
    {

        [ElasticProperty(Name = "expand", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string expand { get; set; }

        [ElasticProperty(Name = "id", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string id { get; set; }

        [ElasticProperty(Name = "self", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string self { get; set; }

        [ElasticProperty(Name = "key", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string key { get; set; }

        [ElasticProperty(Name = "fields", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public Fields fields { get; set; }
    }

    public class RootObject
    {
        public string expand { get; set; }
        public long startAt { get; set; }
        public long maxResults { get; set; }
        public long total { get; set; }
        public List<Issues> issues { get; set; }
    }

    public enum JiraResource
    {
        project
    }

    public class JiraManager
    {
        private const string m_BaseUrl = "http://projects.splasherstech.com:8080/rest/api/2/search?jql=";
        private const string m_BaseUrl1 = "https://artemisweb.beanstalkapp.com/api/changesets.json";
        private const string url1 = "https://artemisweb.beanstalkapp.com/api/repositories.json";
        private string m_Username;
        private string m_Password;

        public JiraManager(string username, string password)
        {
            m_Username = username;
            m_Password = password;
        }

        public string RunQuery(JiraResource resource, string argument = null, string data = null, string method = "GET")
        {

            string url = string.Format("{0}{1}", m_BaseUrl, resource.ToString());

            if (argument != null)
            {
                url = string.Format("{0}{1}", url, argument);

            }

            string result = GetJsonStringData(url, method, data);

            return result;

        }

        public string RunCommitQuery(string argument = null, string data = null, string method = "GET")
        {
            string url = string.Format("{0}", m_BaseUrl1);

            /* Pull changesets
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            request.ContentType = "application/json";
            request.Method = method;*/

            //  read results for changesets
            string result = GetJsonStringData(url, method, data);

            return result;
        }

        public List<Commit> GetCommits()
        {
            List<Commits> projects = new List<Commits>();
            string commitStrings = RunCommitQuery();

            var commitList = JsonConvert.DeserializeObject<List<Commits>>(commitStrings);
            return commitList.Select(x => x.Changeset).ToList();
        }

        private string GetEncodedCredentials()
        {
            string mergedCredentials = string.Format("{0}:{1}", m_Username, m_Password);
            byte[] byteCredentials = UTF8Encoding.UTF8.GetBytes(mergedCredentials);
            return Convert.ToBase64String(byteCredentials);
        }

        private string GetJsonStringData(string url, string method, string data)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.ContentType = "application/json";
                request.Method = method;

                if (data != null)
                {
                    using (StreamWriter writer = new StreamWriter(request.GetRequestStream()))
                    {
                        writer.Write(data);
                    }
                }

                string base64Credentials = GetEncodedCredentials();
                request.Headers.Add("Authorization", "Basic " + base64Credentials);

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                string result = string.Empty;
                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    result = reader.ReadToEnd();
                }

                return result;
            }

        }
    }
}
