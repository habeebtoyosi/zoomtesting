﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearch
{
    [ElasticType(IdProperty = "Id", Name = "sbu")]
    class SBU
    {
        [ElasticProperty(Name = "id", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string id { get; set; }

        [ElasticProperty(Name = "name", Index = FieldIndexOption.NotAnalyzed, Type = FieldType.String)]
        public string name { get; set; }
    }
}
